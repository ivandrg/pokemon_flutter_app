import 'package:flutter_test/flutter_test.dart';
import 'package:pokemon_stacked_app/app/app.locator.dart';

import '../helpers/test_helpers.dart';

void main() {
  group('PokemonServiceTest -', () {
    setUp(() => registerServices());
    tearDown(() => locator.reset());
  });
}
