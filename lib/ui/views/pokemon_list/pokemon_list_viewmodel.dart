import 'package:flutter/material.dart';
import 'package:pokemon_stacked_app/app/app.locator.dart';
import 'package:pokemon_stacked_app/services/pokemon_service.dart';
import 'package:stacked/stacked.dart';

class PokemonListViewModel extends BaseViewModel {
  final _pokemonApi = locator<PokemonService>();

  List get pokemonsList => [pokemons, listPokemonDetails];
  List pokemons = [];
  List urlImgPokemons = [];
  late dynamic pokemonDetails = '';
  List listPokemonDetails = [];
  List allPokemons = [];
  List allPokemonsDetails = [];
  TextEditingController searchController = TextEditingController();

  Future getPokemon() async {
    int cont = 0;
    final response = await _pokemonApi.getPokemon();
    pokemons.addAll(response.results);
    allPokemons.addAll(response.results);
    for (var pokemon in pokemons) {
      String urlPok = await getPokemonDetails(pokemon.url);
      pokemons[cont].image = urlPok;
      cont++;
    }
    rebuildUi();
  }

  Future getPokemonDetails(String url) async {
    pokemonDetails = await _pokemonApi.getPokemonDetails(url);
    listPokemonDetails.add(pokemonDetails);
    allPokemonsDetails.add(pokemonDetails);
    return pokemonDetails.sprites.frontDefault;
  }

  void filterListPokemon() {
    pokemons.clear();
    listPokemonDetails.clear();
    if (searchController.text.isEmpty) {
      pokemons.addAll(allPokemons);
      listPokemonDetails.addAll(allPokemonsDetails);
    } else {
      final listPokemonSearch = allPokemons
          .where((element) => element.name
              .toLowerCase()
              .contains(searchController.text.toLowerCase()))
          .toList();
      final listDetailSearch = allPokemonsDetails
          .where((element) => element.name
              .toLowerCase()
              .contains(searchController.text.toLowerCase()))
          .toList();
      pokemons.addAll(listPokemonSearch);
      listPokemonDetails.addAll(listDetailSearch);
    }
    rebuildUi();
  }
}
