import 'package:flutter/material.dart';
import 'package:pokemon_stacked_app/app/app.router.dart';
import 'package:pokemon_stacked_app/ui/common/animations/fade_animation_widget.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../app/app.locator.dart';
import 'pokemon_list_viewmodel.dart';

class PokemonListView extends StackedView<PokemonListViewModel> {
  PokemonListView({Key? key}) : super(key: key);

  final _navigationService = locator<NavigationService>();
  final int duration = 1000;
  @override
  Widget builder(
    BuildContext context,
    PokemonListViewModel viewModel,
    Widget? child,
  ) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: FadeAnimationWidget(
            delay: 500,
            duration: 3000,
            child: TextFormField(
              controller: viewModel.searchController,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontWeight: FontWeight.w300,
              ),
              decoration: InputDecoration(
                suffixIcon: GestureDetector(
                  onTap: () {
                    viewModel.searchController.clear();
                    viewModel.filterListPokemon();
                  },
                  child: const Icon(Icons.close),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.transparent,
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                ),
                enabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.transparent,
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                ),
                prefixIcon: const Icon(
                  Icons.search,
                  color: Colors.white,
                  size: 24,
                ),
                contentPadding: const EdgeInsets.all(8),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15.0),
                  borderSide: BorderSide.none,
                ),
                hintText: "Buscar",
                hintStyle: const TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.w300,
                ),
                filled: true,
                fillColor: const Color.fromARGB(95, 112, 112, 112),
              ),
              onChanged: (value) {
                viewModel.filterListPokemon();
              },
            ),
          ),
        ),
      ),
      backgroundColor: Theme.of(context).colorScheme.background,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
        child: ListView(
          children: [
            FadeAnimationWidget(
              delay: 1000,
              duration: duration,
              child: const Text(
                'Selecciona Tu',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ),
            FadeAnimationWidget(
              delay: 1500,
              duration: duration,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    'Pokémon',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 37,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Image.asset(
                    'assets/images/poke_ball_icon.png',
                    height: 27,
                  )
                ],
              ),
            ),
            Builder(
              builder: (context) {
                if (viewModel.pokemonsList[0].isEmpty) {
                  return const SizedBox(
                    height: 400,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                } else if (viewModel.pokemonsList.isNotEmpty) {
                  return GridView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 5,
                    ),
                    itemCount: viewModel.pokemonsList[0].length,
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {
                          _navigationService.navigateToPokemonDetailView(
                              details: viewModel.pokemonsList[1][index]);
                        },
                        child: PokemonCardWidget(
                          name: viewModel.pokemonsList[0][index].name,
                          url: viewModel.pokemonsList[0][index].image,
                        ),
                      );
                    },
                  );
                }
                return const SizedBox();
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  PokemonListViewModel viewModelBuilder(
    BuildContext context,
  ) =>
      PokemonListViewModel();

  @override
  void onViewModelReady(PokemonListViewModel viewModel) {
    viewModel.getPokemon();
    super.onViewModelReady(viewModel);
  }
}

class PokemonCardWidget extends StatelessWidget {
  const PokemonCardWidget({
    super.key,
    required this.name,
    required this.url,
  });

  final String name;
  final String url;

  @override
  Widget build(BuildContext context) {
    return FadeAnimationWidget(
      delay: 500,
      duration: 1000,
      child: UnconstrainedBox(
        child: Container(
          width: MediaQuery.sizeOf(context).width * 0.42,
          height: MediaQuery.sizeOf(context).height * 0.18,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            color: Colors.white10,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.network(
                url,
                scale: 0.8,
              ),
              Text(
                name.toString(),
                style: const TextStyle(
                  color: Colors.white70,
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
