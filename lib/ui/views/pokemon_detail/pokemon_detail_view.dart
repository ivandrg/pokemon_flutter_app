import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../app/app.locator.dart';
import 'pokemon_detail_viewmodel.dart';

class PokemonDetailView extends StackedView<PokemonDetailViewModel> {
  final dynamic details;
  PokemonDetailView({Key? key, required this.details}) : super(key: key);
  final _navigationService = locator<NavigationService>();

  @override
  Widget builder(
    BuildContext context,
    PokemonDetailViewModel viewModel,
    Widget? child,
  ) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
        child: ListView(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: UnconstrainedBox(
                child: GestureDetector(
                  onTap: () {
                    _navigationService.back();
                  },
                  child: Container(
                    margin: const EdgeInsets.symmetric(vertical: 15),
                    width: 45,
                    height: 45,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white10,
                    ),
                    child: const Icon(Icons.arrow_back_ios_new),
                  ),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.white54,
              ),
              child: Image.network(
                details.sprites.frontDefault,
                scale: 0.5,
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: Text(
                  details.name.toString(),
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 35,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
            const ListTile(
              title: Text(
                'Tipo',
                style: TextStyle(color: Colors.white),
              ),
            ),
            SizedBox(
              height: 50,
              width: double.infinity,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: details.types.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    child: Chip(
                      label: Text(
                        details.types[index].type.name,
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
            ListTile(
              title: const Text(
                'Habilidades',
              ),
              subtitle: Text(
                details.abilities[0].ability.name,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  PokemonDetailViewModel viewModelBuilder(
    BuildContext context,
  ) =>
      PokemonDetailViewModel();
}
