import 'package:flutter/material.dart';

class FadeAnimationWidget extends StatefulWidget {
  const FadeAnimationWidget({
    Key? key,
    required this.duration,
    required this.delay,
    required this.child,
  }) : super(key: key);
  final int duration;
  final int delay;
  final Widget child;

  @override
  State<FadeAnimationWidget> createState() => _FadeAnimationWidgetState();
}

class _FadeAnimationWidgetState extends State<FadeAnimationWidget>
    with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    Future.delayed(
      Duration(milliseconds: widget.delay),
      () => _controller.forward(),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  late final AnimationController _controller = AnimationController(
    duration: Duration(milliseconds: widget.duration),
    vsync: this,
  );
  late final Animation<double> _animation = CurvedAnimation(
    parent: _controller,
    curve: Curves.decelerate,
  );

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: _animation,
      child: widget.child,
    );
  }
}
