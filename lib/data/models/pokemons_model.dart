// To parse this JSON data, do
//
//     final pokemonListModel = pokemonListModelFromMap(jsonString);

import 'dart:convert';

PokemonListModel pokemonListModelFromMap(String str) =>
    PokemonListModel.fromMap(json.decode(str));

String pokemonListModelToMap(PokemonListModel data) =>
    json.encode(data.toMap());

class PokemonListModel {
  int count;
  String next;
  dynamic previous;
  List<Result> results;

  PokemonListModel({
    required this.count,
    required this.next,
    required this.previous,
    required this.results,
  });

  factory PokemonListModel.fromMap(Map<String, dynamic> json) =>
      PokemonListModel(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results:
            List<Result>.from(json["results"].map((x) => Result.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(results.map((x) => x.toMap())),
      };
}

class Result {
  String name;
  String url;
  String image;

  Result({
    required this.name,
    required this.url,
    required this.image,
  });

  factory Result.fromMap(Map<String, dynamic> json) => Result(
        name: json["name"],
        url: json["url"],
        image: '',
      );

  Map<String, dynamic> toMap() => {
        "name": name,
        "url": url,
      };
}
