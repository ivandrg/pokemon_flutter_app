import 'package:http/http.dart' as http;
import 'package:pokemon_stacked_app/data/models/pokemon_detail.model.dart';
import 'package:pokemon_stacked_app/data/models/pokemons_model.dart';

class PokemonService {
  final url = "https://pokeapi.co/api/v2/pokemon";

  Future getPokemon() async {
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      return pokemonListModelFromMap(response.body);
    } else {
      throw Exception('Failed to load');
    }
  }

  Future getPokemonDetails(String urlDetail) async {
    final response = await http.get(Uri.parse(urlDetail));
    if (response.statusCode == 200) {
      return pokemonDetailModelFromMap(response.body);
    } else {
      throw Exception('Failed to load');
    }
  }
}
